
# coding: utf-8

# In[10]:

import database.Table as Table
import getmanifest.getPermissions as getPermissions
import os
import os.path
from os import listdir
import zipfile
import psycopg2


# In[16]:

def insertFolderToDB(c, db, folder, dbName, malFolder):
    files = zipfile.ZipFile(folder+".zip", 'r')
    files.extractall()
    filenames = [f for f in files.namelist() if f.endswith(".apk")][:99]
    for s in filenames:
        #arch = files.open(s, 'r')
        #try:
        #    print(arch.readlines())
        #except:
        #    print(arch)
        #    continue
        apk = zipfile.ZipFile(s, 'r')
        apk.extract("mainfest.xml")
        manifest = os.path.join(s, [f for f in zipfile.ZipFile(s, 'r').namelist() if f.endswith(".xml")][0])
        print(manifest)
        v = getPermissions.vectorize(manifest)
        print(v)
        Table.insertTable(c, db, dbName, s, v, folder)
    return


# In[17]:

if __name__ == '__main__':
    c,db = Table.getDatabase()
    print("got db")
    s = "SELECT * FROM newTableTest ;"
    insertFolderToDB(c, db, "getmanifest/android-malware-master", "newTableTest", True)
    db.execute(s)
    c.commit()
    print("committed")
    print(db.fetchall())
    db.close()    
    c.close()


# In[ ]:




# In[ ]:




# In[ ]:



